module.exports = {
  env: {
    browser: true,
    es6: true,
  },
  parser: "babel-eslint",
  extends: [
    'airbnb',
  ],
  globals: {
    Atomics: 'readonly',
    SharedArrayBuffer: 'readonly',
  },
  parserOptions: {
    ecmaFeatures: {
      jsx: true,
    },
    ecmaVersion: 2018,
    sourceType: 'module',
  },
  plugins: [
    'react',
  ],
  rules: {
    "arrow-parens": [0],
    "react/jsx-uses-react": "warn",
    "react/jsx-uses-vars": ["error"],
    "no-const-assign": "warn",
    "no-this-before-super": "warn",
    "no-undef": "warn",
    "no-unreachable": "warn",
    "no-unused-vars": "warn",
    "constructor-super": "warn",
    "valid-typeof": "warn",
    strict: 0,
    experimentalDecorators: 0,
    "no-console": 0,
    "no-undefine": 0,
    "react/jsx-filename-extension": [1, { "extensions": [".js", ".jsx"] }],
    "strict": 0,
    "react/jsx-props-no-spreading": "off",
  },
};

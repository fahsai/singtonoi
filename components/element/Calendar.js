import React, { useState, useEffect } from 'react';
import Calendar from 'react-calendar/dist/entry.nostyle';

const CalendarComponent = ({ onChangeDate }) => {
  const [date, setDate] = useState();

  useEffect(() => {
    setDate(new Date());
  }, []);

  return (
    <div>
      <Calendar
        onChange={(value) => onChangeDate(value) + setDate(value)}
        value={date}
        prev2Label={null}
        next2Label={null}
      />
    </div>
  );
};

export default CalendarComponent;

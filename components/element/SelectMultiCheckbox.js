import React, { useState, useEffect } from 'react';
import Select from 'react-select';
import makeAnimated from 'react-select/animated';
import PropTypes from 'prop-types';
import { Colors } from '../../utils';

const options = [
  { value: 'chocolate', label: 'Chocolate' },
  { value: 'strawberry', label: 'Strawberry' },
  { value: 'vanilla', label: 'Vanilla' },
];

const customStyles = {
  placeholder: (base) => ({
    ...base,
    color: Colors.lightGray,
  }),
  singleValue: (base) => ({
    ...base,
    // fontSize: '1.125em'
  }),
  control: (base) => ({
    ...base,
    border: '6px',
    // fontSize: '1.125em',
    // backgroundColor: 'none',
    height: 47,
    padding: 0,
    textOverFlow: 'ellipsis',
    overflow: 'hidden',
  }),
  indicatorsContainer: (base) => ({
    ...base,
    height: 47,
  }),
  indicatorSeparator: () => ({ display: 'none' }),
  dropdownIndicator: (defaultStyles) => ({
    ...defaultStyles,
    // corlor: '#83806F',
  }),
  multiValue: (base) => ({
    ...base,
    backgroundColor: 'none',
  }),
  multiValueLabel: (base) => ({
    fontSize: 14,
    backgroundColor: '#16222F',
    color: 'white',
    overflow: 'hidden',
    textOverFlow: 'ellipsis',
    whiteSpace: 'nowrap',
    borderRightRadius: '0px',
    borderLeftRadius: 16,
    padding: '4px 0px 4px 10px',
    borderTopLeftRadius: '20px',
    borderBottomLeftRadius: '20px',
  }),
  multiValueRemove: (base) => ({
    borderTopRightRadius: '20px',
    borderBottomRightRadius: '20px',
    backgroundColor: '#16222F',
    color: 'white',
    paddingRight: '4px',
  }),
};

const animatedComponents = makeAnimated();
const optionRenderer = props => {
  return (
    <div {...props.innerProps} className="p-0 m-0">
      <div className="d-flex align-items-center p-2 mx-3" style={{ borderBottom: '1px solid #E0E0E0' }}>
        <input className="mr-2" type="checkbox" checked={props.isSelected} />
        <div className="weight-med">{props.data.label}</div>
      </div>
    </div>
  );
};
const MultiValueContainer = ({ selectProps, data }) => {
  const label = data.label;
  const allSelected = selectProps.value;
  const index = allSelected.findIndex(selected => selected.label === label);
  const isLastSelected = index === allSelected.length - 1;
  const labelSuffix = isLastSelected ? '' : ', ';
  const val = `${label}${labelSuffix}`;
  return (
    <div className="d-flex" style={{ overflow: 'hidden' }}>
      <div style={{ whiteSpace: 'nowrap' }}>{val}</div>
    </div>
  );
};

const SelectMultiCheckbox = ({ optionsData, placeholder, fontsize, positionMenu }) => {
  const [selectedOption, setSelectedOption] = useState(null);

  const handleChange = (selected) => {
    setSelectedOption(selected);
  };

  useEffect(() => {

  }, []);

  return (
    <div>
      <Select
        className={fontsize ? `font-${fontsize}` : 'font-16'}
        styles={customStyles}
        value={selectedOption}
        onChange={(selected) => handleChange(selected)}
        options={optionsData || options}
        placeholder={placeholder && placeholder}
        closeMenuOnSelect={false}
        components={{ Option: optionRenderer, MultiValueContainer }}
        menuPlacement={positionMenu || 'bottom'}
        clearable={false}
        hideSelectedOptions={false}
        isMulti
      />
    </div>
  );
};

SelectMultiCheckbox.propTypes = {
  optionsData: PropTypes.arrayOf(PropTypes.oneOfType(
    [PropTypes.number, PropTypes.string],
  )).isRequired,
  placeholder: PropTypes.string.isRequired,
  fontsize: PropTypes.number,
  positionMenu: PropTypes.string,
};

SelectMultiCheckbox.defaultProps = {
  fontsize: 16,
  positionMenu: null,
};
export default SelectMultiCheckbox;

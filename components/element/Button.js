import React from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';

const ButtonView = styled.div`
  height: 52px;
  border-radius: 6px;
  display: flex;
  align-items: center;
  justify-content: center;
  padding: 10px 20px;
`;

const Button = ({ buttonStyle, onPress, children, childrenRight, title, fontStyle }) => (
  <ButtonView className="d-flex justify-content-center click background-gold" style={buttonStyle && buttonStyle} onClick={() => onPress && onPress()}>
    <div className="d-flex align-items-center justify-content-center ">
      {
        children && children
      }
      {
        title && (
          <p className="m-0 font-16 weight-med text-center color-veniceBlue" style={fontStyle}>{title}</p>
        )
      }
      {
        childrenRight && childrenRight
      }
    </div>
  </ButtonView>
);

Button.propTypes = {
  buttonStyle: PropTypes.arrayOf(PropTypes.oneOfType([PropTypes.number, PropTypes.string])),
  onPress: PropTypes.func,
  title: PropTypes.string,
  fontStyle: PropTypes.arrayOf(PropTypes.oneOfType([PropTypes.number, PropTypes.string])),
  children: PropTypes.arrayOf(PropTypes.oneOfType([PropTypes.number, PropTypes.string])),
  childrenRight: PropTypes.arrayOf(PropTypes.oneOfType([PropTypes.number, PropTypes.string])),
};

Button.defaultProps = {
  buttonStyle: null,
  onPress: null,
  title: null,
  fontStyle: null,
  children: null,
  childrenRight: null,
};

export default Button;

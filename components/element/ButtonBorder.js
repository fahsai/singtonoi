import React from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';

const ButtonView = styled.div`
  width: 100%;
  height: 52px;
  border-radius: 6px;
  display: flex;
  align-items: center;
  justify-content: center;
  padding: 10px 20px;
  border: 2px solid #eabe69;
`;

const ButtonBorder = ({ width, wrapperStyle, buttonStyle, onPress, children, childrenRight, title, fontStyle }) => (
  <div style={{ ...wrapperStyle, width }}>
    <ButtonView className="d-flex justify-content-center click" style={buttonStyle && buttonStyle} onClick={() => onPress && onPress()}>
      <div className="d-flex align-items-center justify-content-center ">
        {
          children && children
        }
        {
          title && (
            <p className="m-0 font-8 weight-med text-center color-gold" style={fontStyle}>{title}</p>
          )
        }
        {
          childrenRight && childrenRight
        }
      </div>
    </ButtonView>
  </div>
);

ButtonBorder.propTypes = {
  enable: PropTypes.bool,
  width: PropTypes.any,
  wrapperStyle: PropTypes.object,
  buttonStyle: PropTypes.object,
  onPress: PropTypes.func,
  title: PropTypes.string,
  fontStyle: PropTypes.object,
  children: PropTypes.object,
  childrenRight: PropTypes.object,
};

ButtonBorder.defaultProps = {
  enable: true,
  width: null,
  wrapperStyle: null,
  buttonStyle: null,
  onPress: null,
  fontStyle: null,
  children: null,
  childrenRight: null,
};

export default ButtonBorder;

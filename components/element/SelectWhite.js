import React, { useState, useEffect } from 'react';
import Select from 'react-select';
import PropTypes from 'prop-types';
import { Colors } from '../../utils';

const optionsMockup = [
  { value: 'chocolate', label: 'Chocolate' },
  { value: 'strawberry', label: 'Strawberry' },
  { value: 'vanilla', label: 'Vanilla' },
];

const customStyles = {
  placeholder: (base) => ({
    ...base,
    color: Colors.lightGray,
  }),
  singleValue: (base) => ({
    ...base,
    color: Colors.darkBlue,
    // fontWeight: 'bold',
    // fontSize: '1.125em',
  }),
  control: (base) => ({
    ...base,
    border: '6px',
    // fontSize: '1.125em',
    // backgroundColor: 'none',
    height: 47,
    padding: 0,
    textOverFlow: 'ellipsis',
  }),
  indicatorSeparator: () => ({ display: 'none' }),
  dropdownIndicator: (defaultStyles) => ({
    ...defaultStyles,
    // color: '#83806F',
  }),
};

const SelectWhite = ({ optionsData, placeholder, fontsize, positionMenu }) => {
  const [selectedOption, setSelectedOption] = useState(null);

  const handleChange = (selected) => {
    setSelectedOption(selected);
  };

  useEffect(() => {

  }, []);

  return (
    <div>
      <Select
        className={fontsize ? `font-${fontsize}` : 'font-16'}
        styles={customStyles}
        value={selectedOption}
        onChange={(selected) => handleChange(selected)}
        options={optionsData || optionsMockup}
        placeholder={placeholder && placeholder}
        menuPlacement={positionMenu || 'bottom'}
      />
    </div>
  );
};

SelectWhite.propTypes = {
  optionsData: PropTypes.arrayOf(PropTypes.oneOfType(
    [PropTypes.number, PropTypes.string],
  )).isRequired,
  placeholder: PropTypes.string.isRequired,
  fontsize: PropTypes.number,
  positionMenu: PropTypes.string,
};

SelectWhite.defaultProps = {
  fontsize: 16,
  positionMenu: null,
};
export default SelectWhite;

import React, { useState, useEffect } from 'react';
import Select, { components } from 'react-select';


const options = [
  { value: 'chocolate', label: 'Chocolate' },
  { value: 'strawberry', label: 'Strawberry' },
  { value: 'vanilla', label: 'Vanilla' },
];

const DropdownIndicator = props => {
  return (
    components.DropdownIndicator && (
      <components.DropdownIndicator {...props}>
        {/* <FontAwesomeIcon icon={props.selectProps.menuIsOpen ? "caret-up" : "caret-down"} /> */}
      </components.DropdownIndicator>
    )
  );
};

const customStyles = {
  singleValue: (base) => ({
    ...base,
    color: '#83806F',
  }),
  control: (base) => ({
    ...base,
    border: '0px',
    fontSize: '1.125em',
    fontWeight: 'Bold',
    backgroundColor: 'none',
    height: 40,
    padding: 0,
  }),
  indicatorSeparator: () => ({ display: 'none' }),
  dropdownIndicator: (defaultStyles) => ({
    ...defaultStyles,
    color: '#83806F',
  }),
};

const SelectNoneBorder = ({ optionsData }) => {
  const [selectedOption, setSelectedOption] = useState(null);

  const handleChange = (selected) => {
    setSelectedOption(selected);
  };

  useEffect(() => {
    setSelectedOption(optionsData[0]);
  }, []);

  return (
    <div>
      <Select
        styles={customStyles}
        value={selectedOption}
        components={{ DropdownIndicator }}
        onChange={(selected) => handleChange(selected)}
        options={optionsData}
      />
    </div>
  );
};

export default SelectNoneBorder;

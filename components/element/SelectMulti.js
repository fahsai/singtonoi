import React, { useState, useEffect } from 'react';
import Select from 'react-select';
import makeAnimated from 'react-select/animated';
import PropTypes from 'prop-types';
import { Colors } from '../../utils';

const options = [
  { value: 'chocolate', label: 'Chocolate' },
  { value: 'strawberry', label: 'Strawberry' },
  { value: 'vanilla', label: 'Vanilla' },
];

const customStyles = {
  placeholder: (base) => ({
    ...base,
    color: Colors.lightGray,
  }),
  singleValue: (base) => ({
    ...base,
    fontWeight: 'bold',
    // fontSize: '1.125em'
  }),
  control: (base) => ({
    ...base,
    border: '6px',
    // fontSize: '1.125em',
    // backgroundColor: 'none',
    height: 47,
    padding: 0,
    textOverFlow: 'ellipsis',
    overflow: 'hidden',
  }),
  indicatorsContainer: (base) => ({
    ...base,
    height: 47,
  }),
  indicatorSeparator: () => ({ display: 'none' }),
  dropdownIndicator: (defaultStyles) => ({
    ...defaultStyles,
    // corlor: '#83806F',
  }),
  multiValue: (base) => ({
    ...base,
    backgroundColor: 'none',
  }),
  multiValueLabel: (base) => ({
    fontSize: 14,
    backgroundColor: '#16222F',
    color: 'white',
    overflow: 'hidden',
    textOverFlow: 'ellipsis',
    whiteSpace: 'nowrap',
    borderRightRadius: '0px',
    borderLeftRadius: 16,
    padding: '4px 0px 4px 10px',
    borderTopLeftRadius: '20px',
    borderBottomLeftRadius: '20px',
  }),
  multiValueRemove: (base) => ({
    borderTopRightRadius: '20px',
    borderBottomRightRadius: '20px',
    backgroundColor: '#16222F',
    color: 'white',
    paddingRight: '4px',
  }),
};

const animatedComponents = makeAnimated();
const SelectMulti = ({ optionsData, placeholder, fontsize, positionMenu }) => {
  const [selectedOption, setSelectedOption] = useState(null);

  const handleChange = (selected) => {
    setSelectedOption(selected);
  };

  useEffect(() => {

  }, []);

  return (
    <div>
      <Select
        className={fontsize ? `font-${fontsize}` : 'font-16'}
        styles={customStyles}
        value={selectedOption}
        onChange={(selected) => handleChange(selected)}
        options={optionsData}
        placeholder={placeholder && placeholder}
        closeMenuOnSelect={false}
        components={animatedComponents}
        menuPlacement={positionMenu || 'bottom'}
        isMulti
      />
    </div>
  );
};

SelectMulti.propTypes = {
  optionsData: PropTypes.arrayOf(PropTypes.oneOfType(
    [PropTypes.number, PropTypes.string],
  )).isRequired,
  placeholder: PropTypes.string.isRequired,
  fontsize: PropTypes.number,
  positionMenu: PropTypes.string,
};

SelectMulti.defaultProps = {
  fontsize: 16,
  positionMenu: null,
};
export default SelectMulti;

import React from 'react';
import styled from 'styled-components';

const CardVdoTitle = () => {
  return (
    <CardItem>
      <img className="w-100" src="http://i3.ytimg.com/vi/aKHbqm-D62Y/maxresdefault.jpg" />
      {/* <iframe src="https://player.vimeo.com/video/92455644?rel=0&hd=1" width="300" height="169" frameBorder="0" allow="autoplay; fullscreen" allowFullScreen /> */}

      <div className="py-3 px-3 d-flex justify-content-between">
        <div className="font-18 weight-bold">AIRASIA</div>
        <div className="weight-thin">17 Oct 2019</div>
      </div>
    </CardItem>
  );
};

export default CardVdoTitle;

const AvatarImg = styled.img`
  border-radius: 50%;
  border-radius: 50%;
  border: 3px solid #F2F2F2;
`;

const CardItem = styled.div`
  background-color: #F2F2F2;
  width: 100%;
  border-radius: 10px;
  overflow: hidden;
  height: 100%;
`;

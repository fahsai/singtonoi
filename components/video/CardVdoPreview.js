import React from 'react';
import styled from 'styled-components';

const CardVdoPreview = () => {
  return (
    <a href="/profile">
      <CardItem>
        <img className="w-100" src="http://i3.ytimg.com/vi/aKHbqm-D62Y/maxresdefault.jpg" />
        {/* <iframe src="https://player.vimeo.com/video/92455644?rel=0&hd=1" width="300" height="169" frameBorder="0" allow="autoplay; fullscreen" allowFullScreen /> */}

        <div className="py-2 px-3 position-relative">
          <div className="position-absolute" style={{ marginTop: '-10%', right: '5%', width: '20%' }}>
            <AvatarImg className="w-100" src="https://api.adorable.io/avatars/285/abott@adorable.png" alt="Avatar" />
          </div>

          <div className="font-16">PTT (2019)</div>
          <div className="font-18 weight-bold">NUMTHONG</div>
        </div>
      </CardItem>
    </a>
  );
};

export default CardVdoPreview;

const AvatarImg = styled.img`
  border-radius: 50%;
  border-radius: 50%;
  border: 3px solid #F2F2F2;
`;

const CardItem = styled.div`
  background-color: #F2F2F2;
  width: 100%;
  border-radius: 10px;
  overflow: hidden;
  height: 100%;
`;

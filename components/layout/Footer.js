import React from 'react';

const Footer = () => (
  <div id="footer">
    <div className="container-fluid background-white" style={{ height: 200 }}>
      <div className="row">
        <div className="col-md-6 offset-md-3">
          <div className="font-32 weight-bold pt-3 text-center">Awards</div>
        </div>
      </div>
    </div>

    <div className="container-fluid background-white pt-4" style={{ height: 300, borderTop: '2px solid #16222F' }}>
      <div className="row color-veniceBlue">
        <div className="col-12 col-md-3 px-4">
          <div className="font-18 weight-bold pt-3">SINGTONOIHOUSE</div>
        </div>
        <div className="col-12 col-md-3 px-4">
          <div className="font-18 weight-bold pt-3 pb-4">Singtonoi Company</div>
          <div>
            <div>Feed</div>
            <div className="pt-2">Booking</div>
            <div className="pt-2">About us</div>
            <div className="pt-2">Support</div>
          </div>
        </div>
        <div className="col-12 col-md-3 px-4">
          <div className="font-18 weight-bold pt-3 pb-4">Contact</div>

          <div>
            <div>Singtonoi Company Limited 16 Soi Suwanmanee Samsennoak Huaykwang Bangkok 10310 Thailand</div>
            <div className="pt-3">Tel. +66 2690 4555</div>
            <div>Fax. +66 2690 4567</div>
          </div>
        </div>
        <div className="col-12 col-md-3 px-4">
          <div className="font-18 weight-bold pt-3">Follow me</div>
        </div>
      </div>
    </div>
  </div>
);

export default Footer;

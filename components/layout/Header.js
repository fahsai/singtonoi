import React, { useState, useEffect } from 'react';
import styled, { ThemeProvider } from 'styled-components';
import PropTypes from 'prop-types';

const Container = styled.div`
  padding: 2% 0;
  position: ${(props) => { return props.theme.main === 'white' ? 'absolute' : 'relative' }};
`;
const TextColor = styled.span`
  color: ${(props) => props.theme.main};
`;
const SvgColor = styled.span`
  background: ${(props) => props.theme.main};
  &:after {
    background: ${(props) => props.theme.main};
  }
  &:before {
    background: ${(props) => props.theme.main};
  }
`;

const Header = ({ fixTheme }) => {
  const [mainTheme, setMainTheme] = useState(fixTheme || 'white'); // Change 2 Theme black: white

  const ButtonToggle = () => {
    window.jQuery('#menu-button').click(function (e) {
      const flag = window.jQuery(this).attr('aria-expanded');
      if (flag === 'true') {
        window.jQuery('.lines-button').addClass('close');
        window.jQuery(this).attr('aria-expanded', 'false');

        window.jQuery('.menu-slide').addClass('menu-slide-show');
        window.jQuery('.menu-slide').removeClass('menu-slide-hidden');
        window.jQuery('body').css('overflow', 'hidden');
        window.jQuery('#menu-title').css('color', 'white');
        // window.jQuery('.lines').css('background', 'white');
      } else {
        window.jQuery(this).attr('aria-expanded', 'true');
        window.jQuery('.lines-button').removeClass('close');

        window.jQuery('.menu-slide').removeClass('menu-slide-show');
        window.jQuery('.menu-slide').addClass('menu-slide-hidden');
        window.jQuery('body').css('overflow', 'auto');
        window.jQuery('#menu-title').css('color', mainTheme);
        // window.jQuery('.lines').css('background', mainTheme);


      }
    });
  };

  useEffect(() => {
    ButtonToggle();
  }, []);
  return (
    <ThemeProvider theme={{ main: mainTheme }}>
      <Container className="w-100" style={{ zIndex: 3 }}>
        <div className="container-fluid">
          <div className="row">
            <div className="col-6 font-32 weight-bold color-white">
              <div className="d-none d-lg-block">
                <a href="./"><TextColor>SINGTONOIHOUSE</TextColor></a>
              </div>
            </div>
            <div className="col-6 d-flex justify-content-end">
              <div className="d-flex flex-column align-items-center">
                {
                  mainTheme === 'black'
                    ? <img src="/static/images/icon-b.png" alt="Icon" style={{ width: 32, height: 32 }} />
                    : <img src="/static/images/icon-w.png" alt="Icon" style={{ width: 32, height: 32 }} />
                }
                <TextColor className="font-18 color-white">My Director</TextColor>
              </div>
              <div style={{ width: 60 }} />
            </div>
          </div>
        </div>
      </Container>
      <div className="header-menu">
        <div id="menu-button">
          <button type="button" className="lines-button x" aria-expanded="true">
            <SvgColor className="lines" />
          </button>
          <TextColor id="menu-title" className="font-18 menu-button-title">Menu</TextColor>
        </div>
      </div>
      <div className="menu-slide menu-slide-hidden">
        <div className="text-center">
          <div className="list"><a href="#" className="animated-underline">SEARCH DIRECTOR</a></div>
          <div className="list"><a href="#" className="animated-underline">EXPLORE</a></div>
          <div className="list"><a href="/wishlist" className="animated-underline">WISHLIST</a></div>
          <div className="list"><a href="#" className="animated-underline">ABOUT US</a></div>
        </div>
      </div>
    </ThemeProvider>
  );
};

Header.propTypes = {
  fixTheme: PropTypes.string,
};
Header.defaultProps = {
  fixTheme: 'white',
};

export default Header;

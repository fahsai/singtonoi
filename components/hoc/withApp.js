import React from 'react';
import idx from 'idx';
import { Header, Footer } from '../layout';

const withApp = (WrappedComponent, obj) => {
  const App = ({ isServer, valueFromServer }) => {
    return (
      <div>
        <Header {...valueFromServer} fixTheme={idx(obj, _ => _.fixTheme)} />
        <WrappedComponent {...valueFromServer} />
        <Footer />
      </div>
    );
  };

  App.getInitialProps = async ({ req }) => {
    const isServer = !process.browser;

    return {
      isServer,
      valueFromServer: {},
    };
  };

  return App;
};

export default withApp;

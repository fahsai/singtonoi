import React from 'react';
import styled from 'styled-components';
import { Button } from '../element';
import breakpoint from '../../helpers/breakpoint';

const CardDirectorItem = () => {
  return (
    <CardItem className="py-3 px-3 mb-3">
      <div className="row align-items-md-center">
        <div className="col-3 col-md-1 pr-3">
          <AvatarImg className="w-100" src="https://api.adorable.io/avatars/285/abott@adorable.png" alt="Avatar" />
        </div>
        <div className="col-9 col-md-9 px-0">
          <div className="w-100 d-flex flex-column flex-md-row">
            <WidthLg30>
              <ContentAlign>
                <span className="weight-thin">Style</span>
                <span className="weight-bold">Humour,Drama&Emotional</span>
              </ContentAlign>
            </WidthLg30>
            <WidthLg30>
              <ContentAlign>
                <span className="weight-thin">Product</span>
                <span className="weight-bold">Banking & Insurance,Food & Beverage</span>
              </ContentAlign>
            </WidthLg30>
            <WidthLg20>
              <ContentAlign>
                <span className="weight-thin">Finish Date</span>
                <span className="weight-bold">31 Oct  2019</span>
              </ContentAlign>
            </WidthLg20>
            <WidthLg20>
              <ContentAlign>
                <span className="weight-thin">Channel</span>
                <div className="weight-bold">TVC,Online</div>
              </ContentAlign>
            </WidthLg20>
          </div>
        </div>
        <div className="col-12 col-md-2 pt-2 pt-md-0">
          <Button
            title="BOOKING NOW"
          />
        </div>
      </div>
    </CardItem>
  );
};

export default CardDirectorItem;

const CardItem = styled.div`
  background-color: #FFFFFF;
  width: 100%;
  border-radius: 10px;
  overflow: hidden;
  height: 100%;
  -webkit-box-shadow: 0px 0px 8px -1px rgba(0,0,0,0.12);
  -moz-box-shadow: 0px 0px 8px -1px rgba(0,0,0,0.12);
  box-shadow: 0px 0px 8px -1px rgba(0,0,0,0.12);
`;
const AvatarImg = styled.img`
  border-radius: 50%;
  border-radius: 50%;
  border: 3px solid #F2F2F2;
`;
const ContentAlign = styled.div`
  display: flex;
  flex-direction: column;
  word-break:break-all;
  @media ${breakpoint.sm} {  
    text-align: left;
  }
  @media ${breakpoint.md} {  
    text-align: center;
  }
`;
const WidthLg30 = styled.div`
  @media ${breakpoint.sm} {  
    width: 100%;
    padding-right: 14px;
  }
  @media ${breakpoint.md} {  
    width: 30%;
    padding: 0 6px;
  }
`;
const WidthLg20 = styled.div`
  @media ${breakpoint.sm} {  
    width: 100%;
    padding-right: 14px;
  }
  @media ${breakpoint.md} {  
    width: 20%;
    padding: 0 6px;
  }
`;

import React, { useEffect } from 'react';

const SlideVideo = () => {
  useEffect(() => {
    window.jQuery(document).ready(() => {
      window.jQuery('#mainVideo').owlCarousel({
        items: 1,
        nav: false,
        dots: true,
        video: true,
        loop: true,
        autoHeight: true,
        autoplay: true,
        autoplayTimeout: 3000,
      });
    });
  }, []);

  return (
    <div id="mainVideo" className="owl-carousel owl-theme">
      <div className="fixed-video-aspect"><div className="item-video"><a className="owl-video" href="https://vimeo.com/368436524?autoplay=1"> </a></div></div>
      <div className="fixed-video-aspect"><div className="item-video"><a className="owl-video" href="https://vimeo.com/92204141?autoplay=1"> </a></div></div>
      <div className="fixed-video-aspect"><div className="item-video"><a className="owl-video" href="https://vimeo.com/367954456"> </a></div></div>
      <div className="fixed-video-aspect"><div className="item-video"><a className="owl-video" href="https://vimeo.com/92455644"> </a></div></div>
    </div>
  );
};

export default SlideVideo;

import React, { useEffect } from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';
import { CardVdoPreview } from '../video';

const dataMockup = [1, 2, 3, 4, 5];

const SlideCardVideo = ({ keyOwl }) => {
  useEffect(() => {
    window.jQuery(document).ready(() => {
      window.jQuery(`#cardVideo_${keyOwl}`).owlCarousel({
        items: 4,
        margin: 30,
        nav: true,
        dots: false,
        responsive: {
          0: { items: 2 },
          768: { items: 3 },
          1024: { items: 4 },
        },
      });
    });
  }, []);

  return (
    <div id={`cardVideo_${keyOwl}`} className="cardVideo owl-carousel">
      {
        dataMockup.map((i, index) => (
          <CardVdoPreview />
        ))
      }
    </div>
  );
};

SlideCardVideo.propTypes = {
  keyOwl: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number,
  ]).isRequired,
};

SlideCardVideo.defaultProps = {
  keyOwl: 1,
};
export default SlideCardVideo;

const AvatarImg = styled.img`
  border-radius: 50%;
  border-radius: 50%;
  border: 5px solid #F2F2F2;
`;

const CardItem = styled.div`
  background-color: #F2F2F2;
  width: 100%;
  border-radius: 10px;
`;

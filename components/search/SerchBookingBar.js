import React, { useState, useEffect } from 'react';
import styled from 'styled-components';
import * as moment from 'moment';
import {
  SelectWhite,
  SelectMulti,
  Button,
  Calendar,
} from '../element';

const SearchBookingBar = ({ positionSelect }) => {
  const [finishDateMoment, setFinishDateMoment] = useState('');
  const popperDateShow = () => {
    const reference = window.jQuery('.ref-date');
    const popper = window.jQuery('.popper-date');
    const popperInstance = new Popper(reference, popper, {
      placement: 'top',

    });
    popper.show();
  };
  const popperDateHide = () => {
    const reference = window.jQuery('.ref-date');
    const popper = window.jQuery('.popper-date');
    const popperInstance = new Popper(reference, popper, {
      placement: 'top',

    });
    popper.hide();

    window.jQuery(document).mouseup((e) => {
      const container = window.jQuery('.popper-date');
      if (!container.is(e.target) && container.has(e.target).length === 0) {
        container.hide();
      }
    });
  };
  const onChangeFinishDate = (date) => {
    setFinishDateMoment(moment(date).format('DD MMM YYYY'));
    popperDateHide();
  };

  useEffect(() => {
    popperDateHide();
  }, []);
  return (
    <div className="background-veniceBlue container-fluid" style={{ minHeight: 60 }}>
      <div className="d-none d-lg-block">
        <div className="row align-items-end py-3">
          <div className="col-2">
            <InputWrapper className="background-white">
              <div style={{ width: 30 }}>
                <img src="../static/images/search.png" width={24} alt="search" />
              </div>
              <input
                className="pl-3 w-100 font-16"
                type="text"
                placeholder="Search Director"
                style={{ border: 0 }}
              />
            </InputWrapper>
          </div>

          <div className="col-2 pl-0">
            <div className="weight-med color-white pb-1">Budget</div>
            <SelectWhite
              fontsize={16}
              positionMenu={positionSelect}
              placeholder="Budget"
              optionsData={[
                { value: '1', label: '300,000-500,000฿' },
                { value: '2', label: '600,000-800,000฿' },
                { value: '3', label: '900,000-1,200,000฿' },
                { value: '4', label: 'More Than 1,200,000' },
              ]}
            />
          </div>

          <div className="col-2 pl-0">
            <div className="weight-bold color-white pb-1">Movie Genres</div>
            <SelectMulti
              fontsize={16}
              positionMenu={positionSelect}
              placeholder="Movie Genres"
              optionsData={[
                { value: '1', label: 'Drama' },
                { value: '2', label: 'Comedies' },
                { value: '3', label: 'Action' },
                { value: '4', label: 'Sci-Fi' },
              ]}
            />
          </div>

          <div className="col-2 pl-0">
            <div className="weight-bold color-white pb-1">Finish Date</div>
            <InputWrapper className="background-white ref-date" onClick={() => popperDateShow()}>
              <input
                className="font-16 w-100 background-white"
                type="text"
                placeholder="Finish Date"
                value={finishDateMoment}
                style={{ border: 0 }}
                disabled
              />
              <div className="ml-3" style={{ width: 30 }}>
                <img src="https://cdn3.iconfinder.com/data/icons/linecons-free-vector-icons-pack/32/calendar-512.png" width={24} alt="search" />
              </div>
            </InputWrapper>
            <Popover className="popper-date my-2" style={{ display: 'none' }}>
              <Calendar
                onChangeDate={(date) => onChangeFinishDate(date)}
              />
            </Popover>
          </div>

          <div className="col-2 pl-0">
            <div className="weight-bold color-white pb-1">Movie Type</div>
            <SelectMulti
              positionMenu={positionSelect}
              fontsize={16}
              placeholder="Movie Type"
              optionsData={[
                { value: '1', label: 'TVC' },
                { value: '2', label: 'Internet Film' },
              ]}
            />
          </div>

          <div className="col-2 pl-0">
            <Button
              title="Search"
              buttonStyle={{ height: '47px' }}
            />
          </div>
        </div>
      </div>
      <div className="d-lg-none">
        <div className="color-white py-4">Search Booking for Mobile</div>
      </div>
    </div>
  );
};

export default SearchBookingBar;

const InputWrapper = styled.div`
  height: 47px;
  width: 100%
  border: 1px;
  border-radius: 6px;
  padding: 10px;
  display: flex;
`;

const Popover = styled.div`
  min-height: 200px;
  width: 300px;
  background-color: white;
  border-width: 1px;
  border-radius: 6px;
  padding: 10px;
  z-index: 1;
`;

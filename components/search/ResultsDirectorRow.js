import React from 'react';
import styled from 'styled-components';
import { CardVdoPreview } from '../video';

const ResultsDirectorRow = () => {
  return (
    <div>
      <div className="d-flex flex-row justify-content-end">
        <div className="">More</div>
      </div>
      <div className="row row-eq-height py-3">
        <div className="col-12 col-sm-5 col-md-4 col-lg-3 pb-2 pr-sm-1 pr-lg-3">
          <CardItem>
            <div className="d-flex align-items-center">
              <div style={{ width: '30%' }}>
                <AvatarImg className="w-100" src="https://api.adorable.io/avatars/285/abott@adorable.png" alt="Avatar" />
              </div>
              <div className="pl-2" style={{ width: '70%', wordWrap: 'break-word' }}>
                <Text2Line className="weight-bold font-24">NUMTHONG</Text2Line>
              </div>
            </div>
            <div className="font-18 pt-3">
              <div className="pb-2">
                Movie Type:&nbsp;
              <span className="weight-bold">TVC, Internet Film</span>
              </div>
              <div className="pb-2">
                Movie Genres:&nbsp;
              <span className="weight-bold">Drama, Comedies</span>
              </div>
              <div className="pb-2">
                Finish Date:&nbsp;
              <span className="weight-bold">31 Oct 2019</span>
              </div>
            </div>
          </CardItem>
        </div>
        <div className="col-12 col-sm-7 col-md-8 col-lg-9">
          <div className="row h-100">
            <div className="col-12 col-md-4 pb-1 px-md-1 px-lg-3"><CardVdoPreview /></div>
            <div className="col-12 col-md-4 pb-1 px-md-1 px-lg-3"><CardVdoPreview /></div>
            <div className="col-12 col-md-4 pb-1 px-md-1 px-lg-3"><CardVdoPreview /></div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default ResultsDirectorRow;

const CardItem = styled.div`
  background-color: white;
  width: 100%;
  border-radius: 10px;
  overflow: hidden;
  padding: 18px;
  -webkit-box-shadow: 0px 0px 9px 3px rgba(235,235,235,1);
  -moz-box-shadow: 0px 0px 9px 3px rgba(235,235,235,1);
  box-shadow: 0px 0px 9px 3px rgba(235,235,235,1);
`;
const Text2Line = styled.div`
  overflow: hidden;
  text-overflow: ellipsis;
  display: -webkit-box;
  -webkit-box-orient: vertical;
  -webkit-line-clamp: 2; /* number of lines to show */
  line-height: 22px;        /* fallback */
  max-height: 44px;       /* fallback */
`;
const AvatarImg = styled.img`
  border-radius: 50%;
  border-radius: 50%;
  border: 3px solid #F2F2F2;
`;

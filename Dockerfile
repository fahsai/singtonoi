FROM node:8.11.4-alpine
MAINTAINER Fahsai Sripaew <fahsai@antinode.co.th>

# Create app directory
RUN mkdir -p /usr/src/app
WORKDIR /usr/src/app

COPY package.json /usr/src/app/
RUN npm install

# Bundle app source
COPY . /usr/src/app


RUN npm run build
EXPOSE 3001
CMD [ "npm", "run", "docker" ]
import React from 'react';
import styled from 'styled-components';
import { withApp } from '../components/hoc';
import { Button, ButtonBorder, SelectNoneBorder } from '../components/element';
import { CardVdoTitle } from '../components/video';
import breakpoint from '../helpers/breakpoint';

const profile = () => {
  return (
    <div className="background-snow container-fluid py-4">
      <div className="row">
        <div className="col-12 col-md-4 col-lg-3">
          <CardProfile>
            <ImageDirector src="https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcQdBbT5S4jUf3qO79ztTYU3Z-xcXfGdVM3S1bhk-UpTc_5q71uQ" alt="profile" />
            <div className="color-white p-4 background-veniceBlue">
              <Text1Line className="weight-bold font-26">THANONCHAI</Text1Line>
              <div className="pt-3 weight-thin">
                Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.
            </div>
              <div className="pt-3">
                <span className="weight-thin">Style:&nbsp;</span>
                <TextUnderLine>Humour, Drama&Insurance Food & Beverage</TextUnderLine>
              </div>
              <div className="pt-3">
                <span className="weight-thin">Awards:&nbsp;</span>
                <div className="row pt-1">
                  <div className="col-3"><img className="w-100 px-lg-1 pb-2" src="https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcRCYISL6YjDIm3MGLxcBqlmC3HPOgsx5O2TalmnnZ05FOPsrcoj" alt="award" style={{ objectFit: 'contain' }} /></div>
                  <div className="col-3"><img className="w-100 px-lg-1 pb-2" src="https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcRCYISL6YjDIm3MGLxcBqlmC3HPOgsx5O2TalmnnZ05FOPsrcoj" alt="award" style={{ objectFit: 'contain' }} /></div>
                  <div className="col-3"><img className="w-100 px-lg-1 pb-2" src="https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcRCYISL6YjDIm3MGLxcBqlmC3HPOgsx5O2TalmnnZ05FOPsrcoj" alt="award" style={{ objectFit: 'contain' }} /></div>
                  <div className="col-3"><img className="w-100 px-lg-1 pb-2" src="https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcRCYISL6YjDIm3MGLxcBqlmC3HPOgsx5O2TalmnnZ05FOPsrcoj" alt="award" style={{ objectFit: 'contain' }} /></div>
                  <div className="col-3"><img className="w-100 px-lg-1 pb-2" src="https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcRCYISL6YjDIm3MGLxcBqlmC3HPOgsx5O2TalmnnZ05FOPsrcoj" alt="award" style={{ objectFit: 'contain' }} /></div>
                  <div className="col-3"><img className="w-100 px-lg-1 pb-2" src="https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcRCYISL6YjDIm3MGLxcBqlmC3HPOgsx5O2TalmnnZ05FOPsrcoj" alt="award" style={{ objectFit: 'contain' }} /></div>
                  <div className="col-3"><img className="w-100 px-lg-1 pb-2" src="https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcRCYISL6YjDIm3MGLxcBqlmC3HPOgsx5O2TalmnnZ05FOPsrcoj" alt="award" style={{ objectFit: 'contain' }} /></div>
                  <div className="col-3"><img className="w-100 px-lg-1 pb-2" src="https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcRCYISL6YjDIm3MGLxcBqlmC3HPOgsx5O2TalmnnZ05FOPsrcoj" alt="award" style={{ objectFit: 'contain' }} /></div>
                </div>
              </div>

              <div className="pt-3">
                <Button
                  title="BOOKING NOW"
                />
              </div>

              <div className="pt-3">
                <ButtonBorder
                  title="Save"
                />
              </div>
            </div>
          </CardProfile>
        </div>
        <div className="col-12 col-md-8 col-lg-9 px-md-0">
          <div className="d-flex justify-content-between w-100">
            <div className="d-flex align-items-center">
              <div className="font-18 color-bandicoot pr-2">By Style :</div>
              <div style={{ width: 120 }}>
                <SelectNoneBorder
                  optionsData={[
                    { value: 'All', label: 'All Video' },
                    { value: 'Comedy', label: 'Comedy' },
                  ]}
                />
              </div>
            </div>
            <div className="d-flex align-items-center">
              <div className="font-18 color-bandicoot">By Product :</div>
              <div style={{ width: 120 }}>
                <SelectNoneBorder
                  optionsData={[
                    { value: 'Video', label: 'Video' },
                    { value: 'Film', label: 'Film' },
                  ]}
                />
              </div>
            </div>
          </div>

          <div className="row pt-4">
            <div className="col-12 col-md-6 col-lg-4 pb-4"><CardVdoTitle /></div>
            <div className="col-12 col-md-6 col-lg-4 pb-4"><CardVdoTitle /></div>
            <div className="col-12 col-md-6 col-lg-4 pb-4"><CardVdoTitle /></div>
            <div className="col-12 col-md-6 col-lg-4 pb-4"><CardVdoTitle /></div>
            <div className="col-12 col-md-6 col-lg-4 pb-4"><CardVdoTitle /></div>
            <div className="col-12 col-md-6 col-lg-4 pb-4"><CardVdoTitle /></div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default withApp(profile, { fixTheme: 'black' });

const CardProfile = styled.div`
  border: 1px;
  border-radius: 8px;
  overflow: hidden;
`;
const ImageDirector = styled.img`
  width: 100%;
  object-fit: cover;

  @media ${breakpoint.sm} {  
    height: 450px;
  }
  @media ${breakpoint.md} {  
    height: 350px;
  }
  @media ${breakpoint.lg} {  
    height: 500px;
  }
`;
const Text1Line = styled.div`
  overflow: hidden;
  text-overflow: ellipsis;
  display: -webkit-box;
  -webkit-box-orient: vertical;
`;

const TextUnderLine = styled.span`
  text-decoration: underline;
  font-weight: 500;
`;

import React from 'react';
import styled from 'styled-components';
import { withApp } from '../components/hoc';
import { SerchBookingBar, ResultsDirectorRow } from '../components/search';

const searchResults = () => {
  return (
    <div className="background-snow">
      <SerchBookingBar />
      <div className="container-fluid py-4">
        <div className="font-32 weight-bold">2 Directors</div>

        <ResultsDirectorRow />
        <LineBorder />
        <ResultsDirectorRow />
      </div>
    </div>
  );
};

export default withApp(searchResults, { fixTheme: 'black' });

const LineBorder = styled.div`
  border-top: 2px solid #F2F2F2;
  height: 10px;
  width: 100%;
  margin: 30px 0px;
`;

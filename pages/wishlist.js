import React from 'react';
import { withApp } from '../components/hoc';
import { CardDirectorItem } from '../components/wishlist';

const wishlist = () => {
  return (
    <div className="background-snow container-fluid py-4">
      <div className="weight-bold font-26 pb-4">My Wishlist</div>
      <CardDirectorItem />
      <CardDirectorItem />
    </div>
  );
};

export default withApp(wishlist, { fixTheme: 'black' });

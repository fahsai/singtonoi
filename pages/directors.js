import React, { useEffect } from 'react';
import styled from 'styled-components';
import { withApp } from '../components/hoc';
import { SelectNoneBorder } from '../components/element';
import { SlideVideo, SlideCardVideo } from '../components/slide';
import { SerchBookingBar } from '../components/search';

const AvatarImg = styled.img`
  width: 80px;
  height: 80px;
  border-radius: 50%;
  border-radius: 50%;
  border: 5px solid #D5D0BC;
`;

const Directors = () => {
  const bookingBottom = () => {
    const { offsetHeight } = document.getElementById('serchbookingbar');
    window.jQuery('#footer').css('margin-bottom', offsetHeight);
  };
  useEffect(() => {
    bookingBottom();
  }, []);
  return (
    <div className="w-100">
      <SlideVideo />

      <div className="container-fluid">
        <div className="d-flex position-absolute" style={{ marginTop: '-15%', zIndex: 2 }}>
          <div>
            <AvatarImg src="https://api.adorable.io/avatars/285/abott@adorable.png" alt="Avatar" style={{ width: 80 }} />
          </div>
          <div className="pl-3">
            <div className="color-white font-18">Chaindrite</div>
            <div className="color-white font-32 weight-bold">THANONCHAI</div>
          </div>
        </div>
      </div>

      <div className="container-fluid background-veniceBlue">
        <div className="d-flex flex-md-row flex-column justify-content-between py-sm-2 py-md-4">
          <div className="d-flex align-items-center">
            <div className="font-18 color-bandicoot pr-2">Movie Genres :</div>
            <div style={{ width: 150 }}>
              <SelectNoneBorder
                optionsData={[
                  { value: 'All', label: 'All Video' },
                  { value: 'Comedy', label: 'Comedy' },
                ]}
              />
            </div>
          </div>
          <div className="d-flex align-items-center">
            <div className="font-18 color-bandicoot">View By :</div>
            <div style={{ width: 120 }}>
              <SelectNoneBorder
                optionsData={[
                  { value: 'Video', label: 'Video' },
                  { value: 'Film', label: 'Film' },
                ]}
              />
            </div>
          </div>
        </div>

      </div>

      <div className="container-fluid background-snow pt-4">
        <div className="" style={{ paddingBottom: 40 }}>
          <div className="d-flex flex-row justify-content-between">
            <div className="weight-bold font-26">Action Sci-fi</div>
            <div className="">More</div>
          </div>
          <div className="pt-2">
            <SlideCardVideo
              keyOwl={1}
            />
          </div>
        </div>

        <div style={{ paddingBottom: 40 }}>
          <div className="d-flex flex-row justify-content-between">
            <div className="weight-bold font-26">Action Sci-fi</div>
            <div className="">More</div>
          </div>
          <div className="pt-2">
            <SlideCardVideo
              keyOwl={2}
            />
          </div>
        </div>

        <div style={{ paddingBottom: 40 }}>
          <div className="d-flex flex-row justify-content-between">
            <div className="weight-bold font-26">Action Sci-fi</div>
            <div className="">More</div>
          </div>
          <div className="pt-2">
            <SlideCardVideo
              keyOwl={3}
            />
          </div>
        </div>
      </div>

      <div id="serchbookingbar" className="fixed-bottom">
        <SerchBookingBar positionSelect="top" />
      </div>
    </div>
  );
};

export default withApp(Directors);

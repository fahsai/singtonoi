import React, { useState, useEffect } from 'react';
import styled from 'styled-components';
import * as moment from 'moment';
import { withApp } from '../components/hoc';
import { Colors } from '../utils';
import {
  SelectWhite,
  SelectMulti,
  Button,
  ButtonBorder,
  Calendar,
  SelectMultiCheckbox,
} from '../components/element';

const Home = () => {
  const [finishDateMoment, setFinishDateMoment] = useState('');

  const popperSearchShow = () => {
    const reference = window.jQuery('.ref-textserch');
    const popper = window.jQuery('.popper-textserch');
    const popperInstance = new Popper(reference, popper, {
      placement: 'bottom',

    });
    popper.show();
  };
  const popperSearchHide = () => {
    const reference = window.jQuery('.ref-textserch');
    const popper = window.jQuery('.popper-textserch');
    const popperInstance = new Popper(reference, popper, {
      placement: 'bottom',

    });
    popper.hide();

    window.jQuery(document).mouseup((e) => {
      const container = window.jQuery('.popper-date');
      if (!container.is(e.target) && container.has(e.target).length === 0) {
        container.hide();
      }
    });
  };
  const popperDateShow = () => {
    const reference = window.jQuery('.ref-date');
    const popper = window.jQuery('.popper-date');
    const popperInstance = new Popper(reference, popper, {
      placement: 'bottom',

    });
    popper.show();
  };
  const popperDateHide = () => {
    const reference = window.jQuery('.ref-date');
    const popper = window.jQuery('.popper-date');
    const popperInstance = new Popper(reference, popper, {
      placement: 'bottom',

    });
    popper.hide();
  };
  const onKeySearchDirector = (event) => {
    const text = event.target.value;
    if (text) {
      popperSearchShow();
    } else {
      popperSearchHide();
    }
  };
  const onChangeFinishDate = (date) => {
    setFinishDateMoment(moment(date).format('DD MMM YYYY'));
    popperDateHide();
  };

  useEffect(() => {
    popperSearchHide();
    popperDateHide();
  }, []);
  return (
    <div className="w-100">
      <BackgroundImg>
        <div>
          <div className="col-10 offset-1 col-md-8 offset-md-2 col-xl-6 offset-xl-3 px-0">
            <div className="color-gold font-48 text-center pb-3">
              <span className="weight-med">Director Booking</span>
            </div>

            <div className="row">
              <div className="col-12 ref-textserch">
                <InputWrapper className="background-white">
                  <div style={{ width: 30 }}>
                    <img src="../static/images/search.png" width={24} alt="search" />
                  </div>
                  <Input
                    className="pl-3 w-100"
                    type="text"
                    placeholder="Search director or film title"
                    style={{ border: 0 }}
                    onChange={onKeySearchDirector}
                  />
                </InputWrapper>
              </div>
              <Popover className="popper-textserch mt-2 weight-bold">
                <div className="p-2 d-flex justify-content-between" style={{ borderBottom: '1px solid #F2F2F2' }}>
                  <div>Thanonchai Sornsrivichai</div>
                  <div className="color-bandicoot">TVC, Internet Film</div>
                </div>
                <div className="p-2 d-flex justify-content-between" style={{ borderBottom: '1px solid #F2F2F2' }}>
                  <div>Thanyarak</div>
                  <div className="color-bandicoot">TVC, Internet Film</div>
                </div>
                <div className="p-2 d-flex justify-content-between" style={{ borderBottom: '1px solid #F2F2F2' }}>
                  <div>Thisarakorn</div>
                  <div className="color-bandicoot">TVC, Internet Film</div>
                </div>
              </Popover>
            </div>

            <div className="row">
              <div className="col-12 col-sm-6 pt-14">
                <div className="color-white pb-1">Budget</div>
                <SelectWhite
                  placeholder="Choose your Budget"
                  optionsData={[
                    { value: '1', label: '300,000-500,000฿' },
                    { value: '2', label: '600,000-800,000฿' },
                    { value: '3', label: '900,000-1,200,000฿' },
                    { value: '4', label: '1,300,000-1,500,000฿' },
                    { value: '5', label: 'More Than 1,500,000' },
                  ]}
                />
              </div>
              <div className="col-12 col-sm-6 pt-14 ref-date">
                <div className="color-white pb-1">Finish Date / Release Date</div>
                <InputWrapper className="background-white" onClick={() => popperDateShow()}>
                  <Input
                    className="w-100 background-white"
                    type="text"
                    placeholder="Finish Date"
                    value={finishDateMoment}
                    style={{ border: 0 }}
                    disabled
                  />
                  <div className="ml-3" style={{ width: 30 }}>
                    <img src="/static/images/calendar.png" width={18} alt="finish date" />
                  </div>
                </InputWrapper>
                <Popover className="popper-date mt-2">
                  <Calendar
                    onChangeDate={(date) => onChangeFinishDate(date)}
                  />
                </Popover>
              </div>
            </div>

            <div className="row">
              <div className="col-12 col-sm-6 pt-14">
                <div className="color-white pb-1">Style</div>
                <SelectMulti
                  placeholder="Choose your Style"
                  optionsData={[
                    { value: '1', label: 'Drama' },
                    { value: '2', label: 'Comedies' },
                    { value: '3', label: 'Action' },
                    { value: '4', label: 'Sci-Fi' },
                  ]}
                />
              </div>

              <div className="col-12 col-sm-6 pt-14">
                <div className="color-white pb-1">Product</div>
                <SelectMultiCheckbox
                  placeholder="Choose your Product"
                  optionsData={[
                    { value: '1', label: 'Cosmetics & Beauty & Health Care' },
                    { value: '2', label: 'Corporate Image & CSR' },
                    { value: '3', label: 'Cars, Auto Product & Service' },
                    { value: '4', label: 'Banking & Insurance' },
                  ]}
                />
              </div>
            </div>

            <div className="row pt-4">
              <div className="col-7 pr-1 pr-sm-3">
                <a href="/search-results">
                  <Button
                    title="Search"
                  />
                </a>
              </div>
              <div className="col-5 pl-1 pl-sm-3">
                <a href="/directors">
                  <ButtonBorder
                    title="Skip"
                  />
                </a>
              </div>
            </div>

          </div>
        </div>
      </BackgroundImg>
    </div>
  );
};

export default withApp(Home);

const BackgroundImg = styled.div`
  background: url(/static/images/Background.png) no-repeat center center fixed; 
  -webkit-background-size: cover;
  -moz-background-size: cover;
  -o-background-size: cover;
  background-size: cover;

  @media (min-width: 0px) {
    padding-top: 20%;
    padding-bottom: 20%;
  }
  @media (min-width: 992px) {
    padding-top: 10%;
    padding-bottom: 10%;
  }
`;

const InputWrapper = styled.div`
  height: 47px;
  width: 100%
  border: 1px;
  border-radius: 6px;
  padding: 10px;
  display: flex;
`;

const Input = styled.input`
  color: ${Colors.darkBlue};
  &::-webkit-input-placeholder {
    color: ${Colors.lightGray};
  }
`;

const Popover = styled.div`
  min-height: 200px;
  width: 100%;
  background-color: white;
  border-width: 1px;
  border-radius: 6px;
  padding: 10px;
  z-index: 1;
  display: none;
`;

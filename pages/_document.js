// _document is only rendered on the server side and not on the client side
// Event handlers like onClick can't be added to this file

// ./pages/_document.js
import React from 'react';
import Document, {
  Html,
  Head,
  Main,
  NextScript,
} from 'next/document';
import { ServerStyleSheet } from 'styled-components';

class MyDocument extends Document {
  render() {
    const sheet = new ServerStyleSheet();
    const main = sheet.collectStyles(<Main />);
    const styleTags = sheet.getStyleElement();
    return (
      <Html>
        <Head>
          <link rel="stylesheet" href="/static/css/style.css" />
          <link rel="stylesheet" href="/static/css/menuslide.css" />
          <link rel="stylesheet" href="/static/css/fontface.css" />
          <link rel="stylesheet" href="/static/css/calendar.css" />
          <link rel="stylesheet" href="/static/vendor/node_modules/bootstrap/dist/css/bootstrap.min.css" />
          <link rel="stylesheet" href="/static/vendor/node_modules/owl.carousel/dist/assets/owl.carousel.min.css" />
          <link rel="stylesheet" href="/static/vendor/node_modules/owl.carousel/dist/assets/owl.theme.default.min.css" />
          <script type="text/javascript" src="/static/vendor/node_modules/jquery/dist/jquery.min.js" />
          <script type="text/javascript" src="/static/vendor/node_modules/bootstrap/dist/js/bootstrap.min.js" />
          <script type="text/javascript" src="/static/vendor/node_modules/owl.carousel/dist/owl.carousel.min.js" />
          <script type="text/javascript" src="/static/vendor/node_modules/popper.js/dist/umd/popper.min.js" />
          {styleTags}
        </Head>
        <body>
          {main}
          <NextScript />
        </body>
      </Html>
    );
  }
}

export default MyDocument;

import React from 'react';
import styled from 'styled-components';
import { withApp } from '../components/hoc';
import { Button } from '../components/element';

const wishlist = () => {
  return (
    <div className="background-snow container-fluid py-4">
      <div className="row">
        <div className="col-12 col-md-8 order-2 order-md-1">
          <CardItem>
            <div className="weight-bold font-22 pb-4">Let us know who you are</div>

            <div className="row">
              <div className="col-6">
                <div className="weight-bold pb-1">First Name</div>
                <input
                  className="font-18 w-100"
                  type="text"
                  placeholder="First Name"
                  style={{ border: '1px solid #ecebeb', padding: 6, borderRadius: '4px' }}
                />
              </div>
              <div className="col-6">
                <div className="weight-bold pb-1">Phone Number</div>
                <input
                  className="font-18 w-100"
                  type="text"
                  placeholder="Phone Number"
                  style={{ border: '1px solid #ecebeb', padding: 6, borderRadius: '4px' }}
                />
              </div>
              <div className="col-6 pt-3">
                <div className="weight-bold pb-1">Email Address</div>
                <input
                  className="font-18 w-100"
                  type="text"
                  placeholder="Email Address"
                  style={{ border: '1px solid #ecebeb', padding: 6, borderRadius: '4px' }}
                />
              </div>
            </div>

            <div className="pt-4 d-flex">
              <Button
                title="CONFIRM BOOKING"
                buttonStyle={{ backgroundColor: 'gray' }}
                fontStyle={{ color: 'white' }}
              />
            </div>
          </CardItem>
        </div>
        <div className="col-12 col-md-4 order-1 order-md-2">
          <CardItem>
            <AvatarImg src="https://api.adorable.io/avatars/285/abott@adorable.png" alt="Avatar" style={{ width: '25%' }} />
            <div className="pt-3" style={{ wordBreak: 'break-all' }}>
              <div className="pb-2">
                <span className="weight-thin">Style:&nbsp;</span>
                <span className="weight-bold">Humour,Drama&Emotional</span>
              </div>
              <div className="pb-2">
                <span className="weight-thin">Product:&nbsp;</span>
                <span className="weight-bold">Banking & Insurance , Food & Beverage</span>
              </div>
              <div className="pb-2">
                <span className="weight-thin">Finish Date:&nbsp;</span>
                <span className="weight-bold">31 Oct 2019</span>
              </div>
            </div>
          </CardItem>
        </div>
      </div>
    </div>
  );
};

export default withApp(wishlist, { fixTheme: 'black' });

const CardItem = styled.div`
  background-color: #FFFFFF;
  width: 100%;
  border-radius: 4px;
  overflow: hidden;
  height: 100%;
  padding: 30px;
  -webkit-box-shadow: 0px 0px 8px -1px rgba(0,0,0,0.12);
  -moz-box-shadow: 0px 0px 8px -1px rgba(0,0,0,0.12);
  box-shadow: 0px 0px 8px -1px rgba(0,0,0,0.12);
`;
const AvatarImg = styled.img`
  border-radius: 50%;
  border-radius: 50%;
  border: 3px solid #F2F2F2;
`;
